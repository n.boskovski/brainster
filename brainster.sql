-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2018 at 11:19 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.2.7-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brainster`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `picture` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitle` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `picture`, `title`, `subtitle`, `description`) VALUES
(32, 'https://i1.wp.com/asipnextgen.com/wp-content/uploads/2017/04/instagram-logo-png-transparent-background-download.png?ssl=1', 'Brainster title 1', 'Brainster subtitle 1', 'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters'),
(34, 'https://www.drupal.org/files/project-images/linkedin_circle_logo.png', 'Brainster title 3', 'Brainster subtitle3', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(35, 'http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c53e.png', 'Brainster title 4', 'Brainster subtitle 4', 'Where can I get some?\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a'),
(36, 'https://www.lunapic.com/editor/premade/transparent.gif', 'Brainster title 5', 'Brainster subtitle 5', 'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem I'),
(37, 'http://www.provenrun.com/wp-content/uploads/2012/11/Logo-Prove-Run-Transparent-Background-150ppi-v2.png', 'Brainster title 6', 'Brainster subtitle 6', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.'),
(38, 'https://pngquant.org/pngquant-logo.png', 'Brainster title 7', 'Brainster subtitle 7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It h'),
(39, 'http://www.queness.com/resources/images/png/apple_ex.png', 'Наслов 8', 'Поднаслов 8', 'Во јазиците, текст е нешто што содржи зборови да искаже нешто. Поимот обично има пошироко значење.\r\n\r\nВо лингвистика текстот вклучува два типа на контрасти. Еден е меѓу систем и текст, со тоа што под систем се подразбира способноста на говорниците да к'),
(40, 'http://purepng.com/public/uploads/large/purepng.com-mariomariofictional-charactervideo-gamefranchisenintendodesigner-1701528634653vywuz.png', 'Наслов 9', 'Поднаслов 9', 'Во теорија на литература текст е предметот што се студира, без разлика на тоа дали станува збора за роман, песна, филм, реклама, или било што со лингвистичка компонента. Широкото користење на поимот потекнува од развивањето на семиотиката во текот на');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `email`, `number`, `name`) VALUES
(9, 'nenad_boskovski@yahoo.com', '078244875', 'Axeltra');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
