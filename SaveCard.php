<?php

	require_once "Card.php";

	if (isset($_POST['submit'])) {
		$picture=$_POST['picture'];
		$title=$_POST["title"];
		$subtitle=$_POST["subtitle"];
		$description=$_POST["description"];

		$card = new Card($picture, $title, $subtitle, $description);
		$card->saveCard();
		$card->getAllCards();
		header("Location: adminform.php");
	}



	if (isset($_POST['update'])) {
		$id = $_POST['id'];
		$picture=$_POST['picture'];
		$title=$_POST["title"];
		$subtitle=$_POST["subtitle"];
		$description=$_POST["description"];
		$card = new Card;
		$card->updateCard($id, $picture, $title, $subtitle, $description);
		header("Location: adminform.php");
	}
?>