<?php
	class AdminLogin{
		private $email="admin@admin.com";
		private $password="admin";

		private $error_email="";
		private $error_password="";

		public function getError_email(){
			return $this->error_email;
		}
		public function getError_password(){
			return $this->error_password;
		}

		public function checkPassword($password){
			$password_encrypted = password_hash($this->password, PASSWORD_DEFAULT);
	     	if (password_verify($password, $password_encrypted)) {
	      		return true;
	   		}
	   		return false;
		}

		public function checkEmail($email){
			if ($email==$this->email) {
				return true;
			}
			return false;
		}

		public function Validation($post_email, $post_password){
			if ($this->checkEmail($post_email)==false) {
				$this->error_email="Invalid email";
			}
			if ($this->checkPassword($post_password)==false) {
				$this->error_password="Invalid passwrod";
			}
			if ($this->checkEmail($post_email)==true && $this->checkPassword($post_password)==true) {
				header('Location:adminform.php');
			}
		}
	}

	$admin = new AdminLogin();

	if (isset($_POST['submit'])) {
	$admin_post_email=$_POST["email"];
	$admin_post_password=$_POST["password"];
	$admin->Validation($admin_post_email, $admin_post_password);
	}
?>