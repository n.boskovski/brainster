<?php
	require_once "Connection.php";

	class Company{
		private $name;
		private $email;
		private $number;

		public function __construct($name, $email, $number){
			$this->name=$name;
			$this->email=$email;
			$this->number=$number;
		}

		public function saveCompany(){
			$sql = "INSERT INTO companies (name, email, number) VALUES (:name, :email, :number)";
			$connection = new Connection();
			$db = $connection->connection();
			$stm=$db->prepare($sql);
			$stm->bindParam(":name", $this->name);
			$stm->bindParam(":email", $this->email);
			$stm->bindParam(":number", $this->number);

			return $stm->execute();	
		}
	}
?>