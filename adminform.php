<!DOCTYPE html>
<html>
<head>
	<title></title><meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="adminform.css">
</head>
<body>	
<div class="container">
    <div class="wrapper">
      <h2 class="text-center">Внеси картичка</h2>
      <form class="form-signin" method="POST" action="SaveCard.php"> 
      <label class="color-white">Внеси слика:</label>      
      <input type="text" class="form-control element-margin" name="picture" required autofocus="" />
      <label class="color-white">Внеси наслов:</label>   
      <input type="text" class="form-control element-margin" name="title" required/>
      <label class="color-white">Внеси поднаслов:</label>
      <input type="text" class="form-control element-margin" name="subtitle" required/>
      <label class="color-white">Внеси опис:</label>
      <textarea class="form-control element-margin" rows="5" name="description" required/ maxlength="250"></textarea>
      <button class="btn btn-lg btn-block" type="submit" name="submit">Зачувај</button>
    </form>
  </div>

  <div class= "containter" style="color:white;">
    <h2 class="text-center">Картички</h2>
    <div class="table-responsive">          
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">УРЛ на слика</th>
          <th scope="col">Наслов на картичка </th>
          <th scope="col">Поднаслов на картичка</th>
          <th scope="col">Опис на картичка</th>
        </tr>
      </thead>
      <tbody>
      <?php

        require_once "Card.php";
          
          $objCard = new Card;
          $cards=$objCard->getAllCards();

          foreach ($cards as $card):?>
            <tr>
                <td><?=$card['id'];?></td>
                <td><?=$card['picture'];?></td>
                <td><?=$card['title'];?></td>
                <td><?=$card['subtitle'];?></td>
                <td><?=$card['description'];?></td>
                <td>
                  <a href="edit.php?id=<?=$card['id']?>" class='btn btn-info'>Промени</a>
                </td>
                <td>
                   <a onclick="return confirm('Дали сте сигурни дека сакате да ја избришете оваа картичка')" href="delete.php?id=<?=$card['id']?>" class='btn btn-danger'>Избриши</a>
                </td>
             </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    </div>
  </div>  
</div>
</body>
</html>