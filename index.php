<!DOCTYPE html>
<html>
<head>
	<title></title><meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo-ma">
                	<img src="brainster_logo.png" alt="" class="logo">
                </div>
            </div>
            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav navbar-right mar-r">
                	<li><a class="izdadcha" href="#">Aкадемија за <br>Програмирање</a></li>
                    <li><a class="izdadcha" href="#">Академија за <br>Маркетинг</a></li>
                    <li class="blog-m"><a  class="izdadcha" href="#">Блог</a></li>
                    <li><a  class="izdadcha" href="#" data-toggle="modal" data-target="#myModal">Вработи наши <br>студенти</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="myModal" class="modal fade modal-custom" role="dialog">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	     	 	<div class="modal-header">
	      	 		<button type="button" class="close" data-dismiss="modal">&times;</button>
                	<!-- <img src="brainster_logo.png" alt="" > -->
                	<h2 class="modal-title text-center">Контактирај не</h2>	      	 		
	      		</div>
		     	<div class="modal-body">
		     		<form id="contact-form" class="form" action="SaveCompany.php" method="POST" role="	form">
		              	<div class="form-group">
		                  	<input type="text" class="form-control text-center" id="name" name="name" placeholder="Име на компанија" tabindex="1" required>
		             	</div>                            
		              	<div class="form-group">
		                 	<input type="email" class="form-control text-center" id="email" name="email" placeholder="Email" tabindex="2"required >
		              	</div>                            
		              	<div class="form-group">
		                  	<input type="text" class="form-control text-center" id="subject" name="number" placeholder="Телефон за контакт" tabindex="3" required>
		              	</div>                            
		              	<div class="text-center">
		                  	<button type="submit" class="btn btn-start-order">Испрати</button>
		              	</div>
		          	</form>
		     	</div>
		    	<div class="modal-footer">
	            	<img src="brainster_logo.png" class="modal-logo text-center">
		     	</div>
	    	</div>
	  	</div>
	</div>
    <div class="container-fluid ">
    	<div class="row header ">
    		<div class= header-overlay ">
    			<div class="text-center header-info">
	    			<h1>Brainster.xyz Labs</h1>
 	    			<h4>Проекти на студентите на академиите за програмирање <br> и маркетинг на Brainster</h4>
	    		</div>
    		</div>
    	</div>    	
    </div>


    <?php

        require_once 'Card.php';

        $allProjects = new Card();
        $arrayProjects = $allProjects->getAllCards();
        ?>
        <div class="cardWrapper">
        <?php
        foreach($arrayProjects as $value)
        {
            ?>
            <div class="card caja">
                <div class="cardPic">
                    <img src=" <?= $value['picture']?>">
                </div>
                <h2 class="cardTitle"><?= $value['title']?> </h2> 
                <h3> <?=  $value['subtitle']; ?> </h3>
                <p class="cardDesc"><?= $value['description']?> </p>
            </div>
               <?php
            }
            ?>
        </div>
    <div class="container-fluid">
     	<div class="row">            
            <p class="text-center footer">
            	Made with <span class="glyphicon glyphicon-heart footer-glyphicone"></span> by <img src="brainster_logo.png"><a href="https://www.facebook.com/brainster.co/" class="fb-link">Say Hi!</a> -Terms
            </p>
   		</div>
    </div> 
</body>
</html>		
