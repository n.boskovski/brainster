<?php

    require_once "Connection.php";

    require_once "Card.php";

    $id=$_GET['id'];

    $sql = "DELETE FROM cards WHERE id=:id";
	$connection = new Connection();
	$db = $connection->connection();
	$stm = $db->prepare($sql);
	$stm->bindParam(':id' , $id);
	$stm->execute(); 

	header("Location: adminform.php");

?>