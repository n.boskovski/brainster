<?php
    require_once "Connection.php";
    require_once "Card.php";

    $id=$_GET['id'];
    $objCard = new Card; 
    $card = $objCard->findCard($_GET['id']);
?>

<!DOCTYPE html>
<html>
<head>
	<title></title><meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="adminform.css">
</head>
<body>	
<div class="container">
    <div class="wrapper">
      <h2 class="text-center">Промени картичка</h2>
      <form class="form-signin" method="POST" action="SaveCard.php">
      <label class="color-white">ИД:</label>
      <input type="text" class="form-control element-margin" name="id" placeholder="ID" required autofocus="" value="<?= $card->id;?>" readonly/>
      <label class="color-white">Слика:</label>        
      <input type="text" class="form-control element-margin" name="picture" placeholder="Внеси слика" required autofocus="" value="<?= $card->picture;?>" />
      <label class="color-white">Наслов:</label>
      <input type="text" class="form-control element-margin" name="title" placeholder="Внеси наслов" required/ value="<?= $card->title;?>">
      <label class="color-white">Поднаслов:</label>     
      <input type="text" class="form-control element-margin" name="subtitle" placeholder="Внеси поднаслов" required/ value="<?= $card->subtitle;?>">
      <label class="color-white">Опис:</label> 
      <textarea class="form-control element-margin" rows="5" name="description"required/ maxlength="250"><?= $card->description;?></textarea>
      <button class="btn btn-lg btn-block" type="submit" name="update">Промени</button>
    </form>
  </div>  
</div>
</body>
</html>