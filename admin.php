<?php
include "AdminLogin.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title></title><meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="admin.css">
</head>
<body>	
<div class="container">
    <div class="wrapper">
    <form class="form-signin" method="POST" action="admin.php">       
      <h2 class="form-signin-heading text-center element-margin">Најави се!</h2>
      <input type="email" class="form-control element-margin" name="email" placeholder="Внеси е-mail" required="" autofocus="" />
      <p class="errors"><?php echo $admin->getError_email(); ?></p>
      <input type="password" class="form-control element-margin" name="password" placeholder="Внеси пасворд" required=""/>
      <p class="errors"><?php echo $admin->getError_password(); ?></p>        
      <button class="btn btn-lg btn-block" type="submit" name="submit">Најави се</button>   
    </form>
  </div>
</div>
</body>
</html>		
