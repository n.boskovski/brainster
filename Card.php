<?php

	require_once "Connection.php";

	class Card{

		private $id;
		private $picture;
		private $title;
		private $subtitle;
		private $description;

		public function __construct($picture=null, $title=null, $subtitle=null, $description=null){
			$this->picture=$picture;
			$this->title=$title;
			$this->subtitle=$subtitle;
			$this->description=$description;
		}

		public function saveCard(){
			$sql = "INSERT INTO cards (picture, title, subtitle, description) VALUES (:picture, :title, :subtitle, :description)";
			$connection = new Connection();
			$db = $connection->connection();
			$stm=$db->prepare($sql);
			$stm->bindParam(":picture", $this->picture);
			$stm->bindParam(":title", $this->title);
			$stm->bindParam(":subtitle", $this->subtitle);
			$stm->bindParam(":description", $this->description);

			return $stm->execute();	
		}

		public function getAllCards(){
			$sql = 'SELECT * FROM cards';
			$connection = new Connection();
			$db = $connection->connection();
			$stm=$db->prepare($sql);
			$stm->execute();	
			$cards=$stm->fetchAll(PDO::FETCH_ASSOC);

			return $cards;
		}

		public function findCard($id){
			$sql = "SELECT * FROM cards WHERE id = :id";
			$connection = new Connection();
			$db = $connection->connection();
			$stm=$db->prepare($sql);	
			$stm->bindParam(':id' , $id);
	        $stm->execute();

	       	return $card=$stm->fetch(PDO::FETCH_OBJ); 

   		}

        public function updateCard($id, $picture, $title, $subtitle, $description){
	        $sql = "UPDATE cards SET picture=:picture, title=:title, subtitle=:subtitle, description=:description WHERE id=:id";
	        $connection = new Connection();
			$db = $connection->connection();
	        $stm = $db->prepare($sql);
	        $stm->bindParam(':picture', $picture);
	        $stm->bindParam(':title', $title);
	        $stm->bindParam(':subtitle', $subtitle);
	        $stm->bindParam(':description', $description);
	        $stm->bindParam(':id' , $id);

	        return $stm->execute(); 
	    }

	}
?>